package edu.uoc.pac2;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.pac2.model.BookItem;

/**
 * An activity representing a list of Books. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link BookDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class BookListActivity extends AppCompatActivity {

    private final static String TAG = "BookListActivity";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private SimpleItemRecyclerViewAdapter adapter;
    private DatabaseReference myRef;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book_list);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        mAuth.signInWithEmailAndPassword("edgardo@panchana.com",
                "XXXXXX").addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            //Realiza algo
                            //Descargar los datos
                            //Llamar a su metodo que descarga los datos
                        }
                        else {
                            //Envia notificacion de error
                        }
                    }
                }
        )


        mAuth.signInWithEmailAndPassword("sucorreo", "lacontrasena")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //Llamar a los datos de la Realtime Database
                        }
                        else {
                            //Enviar mensaje de error o alguna notificacion
                        }
                    }
                })


        // ============ INICI CODI A COMPLETAR ===============
        mAuth.signInWithEmailAndPassword(, )
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signIn:onComplete:" + task.isSuccessful());

                        if (task.isSuccessful()) {

                            
                        } else {

                        }
                    }
                });

        // ============ FI CODI A COMPLETAR ===============

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        swipeContainer = findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // ============ INICI CODI A COMPLETAR ===============
                refreshBookList();
                // ============ FI CODI A COMPLETAR ===============
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        RecyclerView recyclerView = findViewById(R.id.book_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        setupRecyclerView(recyclerView);

        if (findViewById(R.id.book_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void suMetodo(){
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();

        //

    }

    private void downloadBooks() {
        swipeContainer.setRefreshing(true);

        myRef = database.getReference("books");
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getBooksFromDataSnapshot(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                getBooksFromDB();
            }
        });

        DatabaseReference connectedRef = database.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (!connected) {
                    getBooksFromDB();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Listener was cancelled");
            }
        });
    }


    private void refreshBookList() {
        if (myRef != null) {
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    getBooksFromDataSnapshot(dataSnapshot);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Failed to read value
                    Log.w(TAG, "Failed to read value.", databaseError.toException());
                    getBooksFromDB();
                }
            });
        } else {
            Log.w(TAG, "My Ref null");
            getBooksFromDB();
        }
    }

    private void getBooksFromDB() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                MyApplication myApplication = (MyApplication) getApplicationContext();
                final List<BookItem> values = myApplication.getBooks();
                BookListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.setItems(values);
                        swipeContainer.setRefreshing(false);
                        Snackbar.make(swipeContainer, "Books updated from DB", Snackbar.LENGTH_LONG).show();
                    }
                });

            }
        });

    }

    private void getBooksFromDataSnapshot(DataSnapshot dataSnapshot) {
        // This method is called once with the initial value and again
        // whenever data at this location is updated.
        GenericTypeIndicator<ArrayList<BookItem>> genericTypeIndicator =
                new GenericTypeIndicator<ArrayList<BookItem>>() {};
        final ArrayList<BookItem> values = dataSnapshot.getValue(genericTypeIndicator);


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                MyApplication myApplication = (MyApplication) getApplicationContext();
                // Save data in database
                for (BookItem bookItem : values) {
                    if (!myApplication.exists(bookItem)) {
                        myApplication.saveBook(bookItem);
                    }
                }
            }
        });

        adapter.setItems(values);
        swipeContainer.setRefreshing(false);
        Snackbar.make(swipeContainer, "Books updated from Firebase", Snackbar.LENGTH_LONG).show();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        adapter = new SimpleItemRecyclerViewAdapter(new ArrayList<BookItem>());
        recyclerView.setAdapter(adapter);
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<BookItem> mValues;
        private final static int EVEN = 0;
        private final static int ODD = 1;

        public SimpleItemRecyclerViewAdapter(List<BookItem> items) {
            mValues = items;
        }

        public void setItems(List<BookItem> items) {
            // ============ INICI CODI A COMPLETAR ===============
            mValues = items;
            notifyDataSetChanged();
            // ============ FI CODI A COMPLETAR ===============
        }

        @Override
        public int getItemViewType(int position) {
            int type;
            if (position % 2 == 0) {
                type = EVEN;
            } else {
                type = ODD;
            }
            return type;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = null;
            if (viewType == EVEN) {
                 view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.book_list_content, parent, false);
            } else if (viewType == ODD) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.book_list_content_odd, parent, false);
            }
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mTitleView.setText(mValues.get(position).title);
            holder.mAuthorView.setText(mValues.get(position).author);

            // ============ INICI CODI A COMPLETAR ===============
            holder.mView.setTag(position);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentPos = (int) v.getTag();
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(BookDetailFragment.ARG_ITEM_ID, currentPos);
                        BookDetailFragment fragment = new BookDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.book_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Bundle animation = ActivityOptions.makeCustomAnimation(BookListActivity.this, R.anim.translate_in_bottom,
                                R.anim.translate_out_bottom).toBundle();
                        Intent intent = new Intent(context, BookDetailActivity.class);
                        intent.putExtra(BookDetailFragment.ARG_ITEM_ID, currentPos);
                        context.startActivity(intent, animation);
                    }
                }
            });
            // ============ FI CODI A COMPLETAR ===============
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mTitleView;
            public final TextView mAuthorView;
            public BookItem mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mTitleView = view.findViewById(R.id.title);
                mAuthorView = view.findViewById(R.id.author);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTitleView.getText() + "'";
            }
        }
    }
}
