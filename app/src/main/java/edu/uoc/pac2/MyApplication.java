package edu.uoc.pac2;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.facebook.stetho.Stetho;

import java.util.List;

import edu.uoc.pac2.model.ApplicationDatabase;
import edu.uoc.pac2.model.BookItem;

/**
 * Created by laura on 08/08/16.
 */
public class MyApplication extends Application {

    private ApplicationDatabase database;

    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);

        database = Room.databaseBuilder(getApplicationContext(),
                ApplicationDatabase.class, "basedatos-app").build();
    }

    public List<BookItem> getBooks(){
        // ============ INICI CODI A COMPLETAR ===============
        return database.bookDao().loadAllBooks();
        // ============ FI CODI A COMPLETAR ===============
    }

    public boolean exists(BookItem bookItem) {
        // ============ INICI CODI A COMPLETAR ===============
        BookItem bookItem1 = database.bookDao().findBookByTitle(bookItem.getTitle());
        return bookItem1 != null;
        // ============ FI CODI A COMPLETAR ===============
    }

    public void saveBook(BookItem bookItem) {
        // ============ INICI CODI A COMPLETAR ===============
        database.bookDao().addBook(bookItem);
        // ============ FI CODI A COMPLETAR ===============
    }
}