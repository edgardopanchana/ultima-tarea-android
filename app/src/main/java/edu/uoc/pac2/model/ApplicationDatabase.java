package edu.uoc.pac2.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by User on 05/08/2018.
 */

@Database(entities = {BookItem.class},version = 1)
public abstract class ApplicationDatabase extends RoomDatabase {
    public abstract BookDao bookDao();
}

