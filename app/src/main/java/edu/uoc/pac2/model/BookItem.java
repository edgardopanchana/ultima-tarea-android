package edu.uoc.pac2.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 *  A book item representing a piece of content.
 * Created by User on 05/08/2018.
 */
@Entity(tableName = "BookItem")
public class BookItem {

    @PrimaryKey
    @NonNull
    public String title;
    public String author;
    public String publicationDate;
    public String description;
    public String urlImage;

    public final boolean equals(BookItem book) {
        if (this.title.equalsIgnoreCase(book.getTitle())) {
            return true;
        }
        return false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublication_date() {
        return publicationDate;
    }

    public void setPublication_date(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl_image() {
        return urlImage;
    }

    public void setUrl_image(String urlImage) {
        this.urlImage = urlImage;
    }

}

