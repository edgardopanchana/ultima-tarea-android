package edu.uoc.pac2.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by User on 05/08/2018.
 */
@Dao
public interface BookDao {

    @Query("SELECT * FROM BookItem")
    List<BookItem> loadAllBooks();

    @Query("SELECT * FROM BookItem WHERE title LIKE :titleBook LIMIT 1")
    BookItem findBookByTitle(String titleBook);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long addBook(BookItem bookItem);

}
